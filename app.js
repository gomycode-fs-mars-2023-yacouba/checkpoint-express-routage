const express = require('express');
const app = express();

// Middleware pour vérifier l'heure de la demande
const checkWorkingHours = (req, res, next) => {
  const currentDate = new Date();
  const dayOfWeek = currentDate.getDay();
  const currentHour = currentDate.getHours();
  const isWeekday = dayOfWeek >= 1 && dayOfWeek <= 5;
  const isWorkingHours = isWeekday && currentHour >= 9 && currentHour < 17;

  if (!isWorkingHours) {
    res.send('L\'application web n\'est disponible que pendant les heures de travail (du lundi au vendredi, de 9h à 17h).');
  } else {
    next();
  }
};

// Configuration de l'application Express
app.set('view engine', 'pug');
app.use(express.static('public'));

// Routes
app.get('/', checkWorkingHours, (req, res) => {
  res.render('index');
});

app.get('/services', checkWorkingHours, (req, res) => {
  res.render('services');
});

app.get('/contact', checkWorkingHours, (req, res) => {
  res.render('contact')
});

// Démarrage du serveur
app.listen(3000, () => {
  console.log('Le serveur est en écoute sur le port 3000.');
});
